const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
//body parser
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    console.log("CHAMADO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/login', (req, res) => {
    console.log("CHAMADO VIA GET - consulta login");
    console.log(req.query);
    let email = req.query.email;
    let senha = req.query.password;
    let retorno;

    retorno = "<h1>E-mail: </h1>"+ email + "<br>";
    retorno += "<h1>Senha: </h1>"+ senha + "<br>";
    res.send(retorno);
});

app.post('/login',(req, res)=>{
    console.log("CHAMADO VIA POST - post cadastro");
    console.log(req.body);

    let nome = req.body.fname;
    let sobrenome = req.body.pname;
    let email2 = req.body.emailuser;
    let senha2 = req.body.passworduser;
    let aniversariouser = req.body.aniversario;
    let genero = req.body.sex;
    let retornonovo;

    retornonovo = "<h1>Nome: </h1>"+ nome + "<br>";
    retornonovo += "<h1>Sobrenome: </h1>"+ sobrenome + "<br>";
    retornonovo += "<h1>Email: </h1>"+ email2 + "<br>";
    retornonovo += "<h1>Senha: </h1>"+ senha2 + "<br>";
    retornonovo += "<h1>Aniversário: </h1>"+ aniversariouser + "<br>";
    retornonovo += "<h1>Gênero: </h1>"+ genero + "<br>";
    res.send(retornonovo);
});




app.listen(3000, function () {
    console.log("projeto iniciado na porta 3000");
});